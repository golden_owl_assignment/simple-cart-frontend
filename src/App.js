import { useEffect, useState } from 'react';
import './App.css';
import Card from './components/Card/Card';
import axios from 'axios';

function App() {
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState(() => {
    const storedCart = localStorage.getItem('cart');
    return storedCart ? JSON.parse(storedCart) : [];
  });
  useEffect(() => {
    //Fetch the api using axios
    axios
      .get('https://simple-cart-backend.onrender.com/api/v1/products')
      .then(function (response) {
        setProducts(response.data);
      });
  }, []);

  function updateCart(newCart) {
    // Update localStorage with the new cart items
    localStorage.setItem('cart', JSON.stringify(newCart));

    //update cart
    setCart([...newCart]);
  }
  return (
    <div className="app-container">
      <Card
        className=""
        cartTitle={'1'}
        products={products}
        cart={cart}
        updateCart={updateCart}
      ></Card>
      <Card
        cartTitle={'2'}
        products={products}
        cart={cart}
        updateCart={updateCart}
      ></Card>
    </div>
  );
}

export default App;
