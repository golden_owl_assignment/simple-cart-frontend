import { OurProducts } from '../OurProducts/OurProduct';
import { YourCart } from '../YourCart/YourCart';
import { YourCartHeader } from '../YourCart/YourCartHeader';
import './card.style.css';
import nikeIco from '../../assets/nike.png';
export default function Card({ cartTitle, products, cart, updateCart }) {
  return (
    <div className="card-container">
      <div className="card-spot"></div>
      <div className="card-header">
        <img alt="nike-ico" className="nike-ico" src={nikeIco}></img>
        {cartTitle === '1' ? (
          <h2>Our Product</h2>
        ) : (
          <YourCartHeader cart={cart} />
        )}
      </div>
      <div className="card-body">
        {cartTitle === '1' ? (
          <OurProducts
            products={products}
            cart={cart}
            updateCart={updateCart}
          />
        ) : (
          <YourCart cartList={cart} updateCart={updateCart} />
        )}
      </div>
    </div>
  );
}
