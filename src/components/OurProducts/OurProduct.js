import { ProductList } from './ProductList';

export function OurProducts({ products, updateCart, cart }) {
  return (
    <div>
      {products.map((product, index) => {
        return (
          <ProductList
            cart={cart}
            key={index}
            product={product}
            handleCart={updateCart}
          ></ProductList>
        );
      })}
    </div>
  );
}
