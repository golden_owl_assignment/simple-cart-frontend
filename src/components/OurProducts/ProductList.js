import { useEffect, useState } from 'react';
import './OurProduct.style.css';
import checkIco from '../../assets/check.png';
export function ProductList({ product, cart, handleCart }) {
  const [isInCart, setIsInCart] = useState(false);
  useEffect(() => {
    let existingProduct = cart.find((item) => item.id === product.id);
    if (existingProduct) {
      setIsInCart(true);
    } else {
      // If the product is not in the cart, add it with the default amount
      setIsInCart(false);
    }
  }, [cart, product.id]);
  function addToCart() {
    setIsInCart(!isInCart);

    let existingProduct = cart.find((item) => item.id === product.id);
    if (existingProduct) {
      // If the product is already in the cart, increment the amount
      existingProduct.data.amount++;
    } else {
      // If the product is not in the cart, add it with the default amount
      cart.push({ id: product.id, data: product, amount: 1 });
    }
    const newCart = [...cart];
    handleCart(newCart);
  }
  return (
    <div className="product-container">
      <div
        className="image-container"
        style={{ backgroundColor: product.color }}
      >
        <img className="product-image" alt="#" src={product.image}></img>
      </div>
      <h3 className="product-name">{product.name}</h3>
      <div className="product-description">{product.description}</div>
      <div className="add-to-cart-container">
        <h3>${product.price.toFixed(2)}</h3>
        {!isInCart ? (
          <button className="add-to-cart-button" onClick={addToCart}>
            Add to Cart
          </button>
        ) : (
          <div className="check-container">
            <img alt="#" className="check-ico" src={checkIco}></img>
          </div>
        )}
      </div>
    </div>
  );
}
