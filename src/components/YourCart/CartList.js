import { useState } from 'react';
import './yourCart.style.css';
import deleteIco from '../../assets/trash.png';
export function CartList({ product, productAmount, updateAmount }) {
  const [amount, setAmount] = useState(productAmount);

  function handleAmount(quantity) {
    const cart = JSON.parse(localStorage.getItem('cart')) || [];
    const existingProduct = cart.find(
      (item) => item.data.name === product.name
    );
    if (existingProduct) {
      // If the product is already in the cart, update the amount (might be add or subtract)
      existingProduct.amount += quantity;

      if (existingProduct.amount <= 0) {
        //help me remove if amount <=0
        const indexToRemove = cart.indexOf(existingProduct);
        if (indexToRemove !== -1) {
          cart.splice(indexToRemove, 1);
        }
      }
      setAmount(amount + quantity);
    } else {
      // If the product is not in the cart, add it with the default amount
      cart.push({ id: product.id, data: product, amount: 1 });
      setAmount(1);
    }

    // Update localStorage with the new cart items and update cart
    updateAmount(cart);
  }

  function remove() {
    const cart = JSON.parse(localStorage.getItem('cart')) || [];
    const existingProduct = cart.find(
      (item) => item.data.name === product.name
    );
    if (existingProduct) {
      handleAmount(parseInt(productAmount) * -1);
    }
  }
  return (
    <div className="item-container">
      <div className="left-item-container">
        <div
          className="item-background"
          style={{ backgroundColor: product.color }}
        ></div>
        <img alt="#" src={product.image} className="item-image"></img>
      </div>
      <div className="right-item-container">
        <h5>{product.name}</h5>
        <h3>{product.price.toFixed(2)}</h3>
        <div className="modify-container">
          <button className="modify-button" onClick={() => handleAmount(-1)}>
            -
          </button>
          <div>{amount}</div>
          <button className="modify-button" onClick={() => handleAmount(1)}>
            +
          </button>

          <div className="delete-button" onClick={() => remove()}>
            <img src={deleteIco} alt="Delete" className="delete-ico"></img>
          </div>
        </div>
      </div>
    </div>
  );
}
