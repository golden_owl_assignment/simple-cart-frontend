import { CartList } from './CartList';
import './yourCart.style.css';

export function YourCart({ cartList, updateCart }) {
  return (
    <div className="your-cart-container">
      {cartList.length === 0 && <p> Your Cart is empty</p>}
      {cartList.map((cart, index) => {
        return (
          <CartList
            product={cart.data}
            productAmount={cart.amount}
            key={index}
            updateAmount={updateCart}
          ></CartList>
        );
      })}
    </div>
  );
}
