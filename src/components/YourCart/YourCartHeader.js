export function YourCartHeader({ cart }) {
  //calculate total sum from cart
  let sum = 0;
  cart.forEach((item) => {
    sum += item.data.price * item.amount;
  });

  return (
    <div className="cart-header">
      <h2>Your Cart</h2>
      <h2 className="TotalSum">${sum.toFixed(2)}</h2>
    </div>
  );
}
